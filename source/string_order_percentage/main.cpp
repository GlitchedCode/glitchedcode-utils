#include <iostream>
#include <string>

inline float string_order_percentage(std::string source)
{
  if(source.size() < 2) return 0;
  float ret = 0;
  unsigned long int rightPositionCount = 0;
  char current[2] = { source[0], (char)255 };
  if(current[0] > 64 && current[0] < 91) current[0] += 32;
  for(auto c = 0uL; c < source.size() - 1; c++)
  {
    current[1] = source[c + 1];
    if(current[1] > 64 && current[1] < 91) current[1] += 32;
    if(current[0] <= current[1]) rightPositionCount++;
    current[0] = current[1];
  }
  std::cout << rightPositionCount << std::endl;
  if(rightPositionCount) rightPositionCount++;
  ret = (float)rightPositionCount / (float)(source.size());
  ret *= 100;
  return ret;
}

int main()
{
  std::string in;
  std::cin >> in;
  std::cin.sync();
  float out = string_order_percentage(in);
  std::cout << out;
  return 0;
}
