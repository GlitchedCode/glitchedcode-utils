# Makefile for the glitched-utils repo WORK IN PROGRESS DO NOT EXECUTE
export
CXX := g++
PLATFORM := linux
ARCH := x64
BINEXT := out
OBJDIR := ./build/obj
MKDIR_P := mkdir -p
THIS_FILE := $(lastword $(MAKEFILE_LIST))
OUT_PREFIX := ./build/out
MAKE_TARGET := $(MAKE) --no-print-directory -f $(THIS_FILE)

LIBS :=

ifeq ($(OS),Windows_NT)
    CCFLAGS += -D_WIN32
		BINEXT := exe
		PLATFORM := windows
		LIBS := $(LIBS_WINDOWS)
    ifeq ($(PROCESSOR_ARCHITEW6432),AMD64)
        CCFLAGS += -DAMD64
				ARCH := x64
    else
        ifeq ($(PROCESSOR_ARCHITECTURE),AMD64)
            CCFLAGS += -DAMD64
						ARCH := x64
        endif
        ifeq ($(PROCESSOR_ARCHITECTURE),x86)
            CCFLAGS += -DIA32
						ARCH := x86
        endif
    endif
else
    UNAME_S := $(shell uname -s)
    ifeq ($(UNAME_S),Linux)
        CCFLAGS += -DLINUX
				BINEXT := out
				LIBS := $(LIBS_LINUX)
    endif
    ifeq ($(UNAME_S),Darwin)
        CCFLAGS += -DOSX
				BINEXT := out
				LIBS := $(LIBS_LINUX)
    endif
    UNAME_P := $(shell uname -p)
    ifeq ($(UNAME_P),x86_64)
        CCFLAGS += -DAMD64
				ARCH := x64
    endif
    ifneq ($(filter %86,$(UNAME_P)),)
        CCFLAGS += -DIA32
				ARCH := x86
    endif
    ifneq ($(filter arm%,$(UNAME_P)),)
        CCFLAGS += -DARM
				ARCH := arm
    endif
endif

#include paths
INCLUDE:=-I./source/ -I./lib/include/
LIBDIR:=-L./lib/lib/$(PLATFORM)-$(ARCH)/

#compiler flags
#debug
CFLAGS:= -std=c++17 -Wall -Werror -g3 -ggdb $(INCLUDE) $(LIBDIR)
#release
#CFLAGS_RELEASE:= -std=c++17 -Wall -Werror -O3 $(INCLUDE) $(LIBDIR)

#########################################################
# Main makefile targets																	#
all:
	@$(MAKE_TARGET) string_order_percentage

string_order_percentage:
	@$(MAKE_TARGET) print_compile_info
	$(CXX) source/$@/main.cpp -o build/$@.$(BINEXT) $(CFLAGS) $(CCFLAGS) $(LIBS)
#																												#
#########################################################


print_compile_info:
	@echo Compiling $(BUILD_CONF) configuration.
	@echo CFLAGS: $(CFLAGS)
	@echo CCFLAGS: $(CCFLAGS)
	@echo LIBS: $(LIBS)
	@echo Output extension: $(BINEXT)

# Build command
build: $(OUT_PREFIX)-$(BUILD_CONF).$(BINEXT) $(patsubst %, $(OBJDIR)/$(BUILD_CONF)/%, $(OBJ))
	@$(MKDIR_P) $(dir $<)
	$(CXX) $(filter %.o, $($^)) -o $(OUT_PREFIX)-$(BUILD_CONF).$(BINEXT) $(CFLAGS) $(CCFLAGS) $(LIBS)

# Object file generation
$(OBJDIR)/$(BUILD_CONF)/%.o: %.cpp $(DEPS)
	@printf "Generating $@ ... "
	@$(MKDIR_P) $(dir $@)
	@$(CXX) -c -o $@ $(patsubst %, ./source/%, $<) $(CFLAGS) $(CCFLAGS)
	@printf "Done.\n"

.PHONY:

clean:
	rm -rf ./build/*
